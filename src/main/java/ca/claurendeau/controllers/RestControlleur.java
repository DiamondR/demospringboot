package ca.claurendeau.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.repository.AnimalOwnerRepository;

@RestController
public class RestControlleur {
    
    @Autowired
    private AnimalOwnerRepository animalOwnerRepository; 
    
    @GetMapping(value = "/monurl")
    public @ResponseBody List<AnimalOwner> monUrl(@RequestParam String lastName) {
        List<AnimalOwner> animalOwners = animalOwnerRepository.findByLastName(lastName);
        return animalOwners;
    }

}
