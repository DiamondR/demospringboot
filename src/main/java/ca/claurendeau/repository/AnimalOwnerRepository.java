package ca.claurendeau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.AnimalOwner;

public interface AnimalOwnerRepository extends JpaRepository<AnimalOwner, Long> {
    
    List<AnimalOwner> findByLastName(String lastName);
    List<AnimalOwner>  findByLastNameAndFirstName(String lastName, String firstName);

}
