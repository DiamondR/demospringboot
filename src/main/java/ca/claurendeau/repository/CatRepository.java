package ca.claurendeau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.domaine.Cat;

public interface CatRepository extends JpaRepository<Cat, Long> {
    
    List<Cat> findBy(String animalName);

}
